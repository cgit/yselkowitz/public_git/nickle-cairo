Name:           nickle-cairo
Version:        1.8.1
Release:        1%{?dist}
Summary:        cairo bindings for Nickle
License:        LGPLv2.1 or MPLv1.1
URL:            http://cairographics.org/cairo-nickle/
Source0:        http://ftp.debian.org/debian/pool/main/c/cairo-5c/cairo-5c_%{version}.tar.gz

BuildRequires:  nickle-devel
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(librsvg-2.0)
BuildRequires:  pkgconfig(x11)
Requires:       nickle%{_isa}

%description
Cairo-5c provides a simple binding for the cairo graphics library within the
Nickle programming environment.

%prep
%setup -n cairo-5c-%{version}


%build
%configure --disable-static
%make_build


%install
%make_install exampledir=%{_pkgdocdir}/examples
find %{buildroot} -name '*.la' -delete


%files
%license COPYING*
%doc AUTHORS ChangeLog README
%{_libdir}/libcairo-5c.so*
%{_datadir}/nickle/cairo.5c
%{_datadir}/nickle/nichrome*.5c
%{_mandir}/man3/cairo-5c.3*
%doc %{_pkgdocdir}/examples


%changelog
* Tue Aug  9 2016 Yaakov Selkowitz <yselkowi@redhat.com>
- Initial release
